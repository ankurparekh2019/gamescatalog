﻿using System;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using NewtonAssignment.Models.DBModels;
using NewtonAssignment.Models;
using NewtonAssignment.Repository;
using System.Collections.Generic;
using System.Linq;

namespace NewtonAssignment.Tests
{
    [TestFixture]
    public class CategoryRepoDataTest
    {

        private Mock<DbSet<Category>> category;
        private Mock<GamesDBContext> mockGameContext;
        private CategoryRepository categoryRepository;

        [SetUp]
        public void SetUp()
        {
            category = new Mock<DbSet<Category>>();
            SetUpCategoryData();
            mockGameContext = new Mock<GamesDBContext>();
            mockGameContext.Setup(c => c.Category).Returns(category.Object);
            categoryRepository = new CategoryRepository(mockGameContext.Object);
        }

        private void SetUpCategoryData()
        {
            var data = new List<Category>
            {
                new Category { CategoryID = 1, CategoryName = "Action" },
               new Category { CategoryID = 2, CategoryName = "Adventure" },
               new Category { CategoryID = 3, CategoryName = "Simulation" },
               new Category { CategoryID = 4, CategoryName = "Sports" },
               new Category { CategoryID = 5, CategoryName = "Strategy" },
               new Category { CategoryID = 6, CategoryName = "Puzzle" }
            }.AsQueryable();

            category.As<IQueryable<Category>>().Setup(m => m.Provider).Returns(data.Provider);
            category.As<IQueryable<Category>>().Setup(m => m.Expression).Returns(data.Expression);
            category.As<IQueryable<Category>>().Setup(m => m.ElementType).Returns(data.ElementType);
            category.As<IQueryable<Category>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
        }

        [Test]
        public void Get_All_Categories()
        {

            var categoryMasters = categoryRepository.GetCategories();

            Assert.AreEqual(6, categoryMasters.Count);
            Assert.AreEqual("Adventure", categoryMasters[1].CategoryName);



        }
    }
}
