﻿using System;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using NewtonAssignment.Models.DBModels;
using NewtonAssignment.Models;
using NewtonAssignment.Repository;
using System.Collections.Generic;
using System.Linq;

namespace NewtonAssignment.Tests
{
    [TestFixture]
    public class GamesDataRepoTest
    {
        private Mock<DbSet<GameMaster>> gameMaster;
        private Mock<DbSet<GameDetails>> gameDetails;
        private Mock<GamesDBContext> mockGameContext;
        private GamesRepository gamesRepository;

        [SetUp]
        public void SetUp()
        {
            gameMaster = new Mock<DbSet<GameMaster>>();
            gameDetails = new Mock<DbSet<GameDetails>>();
            mockGameContext = new Mock<GamesDBContext>();
            mockGameContext.Setup(x => x.GameMaster.Add(It.IsAny<GameMaster>())).Returns((GameMaster g) => g);
            mockGameContext.Setup(x => x.GameDetails.Add(It.IsAny<GameDetails>())).Returns((GameDetails d) => d);
        }


        [Test]
        public void Create_GameMaster_With_GameDetails()
        {
            gamesRepository = new GamesRepository(mockGameContext.Object);
            var GameDetails = new List<GameDetails>();
            GameDetails.Add(new GameDetails()
            {
                Edition = "Silver",
                AgeRestriction = "14+",
                GameID = 1,
                GameDetailsID = 1,
                Language = "English",
                Platform = "PS3",
                Price = 25,
                Publisher = "EA Games",
                ReleasedDate = DateTime.Now
            });

            var Category = new Category()
            {
                CategoryID = 1,
                CategoryName = "Action"
            };

            var result = gamesRepository.InsertGameDetails(new GameMaster()
            {
                GameID = 1,
                GameName = "Game A",
                CategoryID = 1,
                Description = "New Game A",
                GameDetails = GameDetails,
                Category = Category
            });

            //Check Context.savechnages called for 2 times, one for gameMaster and one for gameDetails
            mockGameContext.Verify(m => m.SaveChanges(), Times.Exactly(2));

            Assert.AreEqual(true, result);


        }

        [Test]
        public void GetAll_GameMaster_GameDetails()
        {

            SetupGameMasterData();
            gamesRepository = new GamesRepository(mockGameContext.Object);

            var gameMastersList = gamesRepository.GetGameMasterDetails();

            Assert.AreEqual(3, gameMastersList.Count);
            Assert.AreEqual("GameA", gameMastersList[0].GameName);
            Assert.AreEqual("New Game Adventure", gameMastersList[1].Description);
            Assert.AreEqual("Diamond", gameMastersList[2].GameDetails[0].Edition);
        }

        [Test]
        public void Get_GameMaster_By_ID()
        {
            SetupGameMasterData();
            gamesRepository = new GamesRepository(mockGameContext.Object);

            var gameMasters = gamesRepository.GetGameMasterDetailsById(1);

            Assert.AreEqual(1, gameMasters.GameID);

            Assert.AreEqual("English", gameMasters.GameDetails[0].Language);

        }


        [Test]
        public void Update_GameMaster_GameDetails()
        {
            SetupGameMasterData();
            SetupGameDetailsData();
            gamesRepository = new GamesRepository(mockGameContext.Object);

            var updateGameMaster = new GameMaster()
            {
                GameID = 1,
                GameName = "GameA",
                Description = "New Game Action Adventure",
                CategoryID = 1,
                GameDetails = new List<GameDetails>(){ new GameDetails() {GameDetailsID = 1, GameID = 1,Edition = "Silver",
                        AgeRestriction = "14+",Language = "English",Platform = "PS3",Price = 35,Publisher = "EA Games",
                    ReleasedDate = DateTime.Now } }
            };

            var result = gamesRepository.UpdateGameDetails(updateGameMaster);
            mockGameContext.Verify(m => m.SaveChanges(), Times.Once);

            Assert.AreEqual(true, result);



        }
        [Test]
        public void Delete_GameMaster()
        {

            SetupGameMasterData();
            SetupGameDetailsData();
            gamesRepository = new GamesRepository(mockGameContext.Object);

            var result = gamesRepository.DeleteGameMaster(3);
            mockGameContext.Verify(m => m.SaveChanges(), Times.Once);

            Assert.AreEqual(true, result);

        }
        [Test]
        public void Delete_GameDetails()
        {

            SetupGameDetailsData();
            gamesRepository = new GamesRepository(mockGameContext.Object);

            var result = gamesRepository.DeleteGameDetails(3);

            mockGameContext.Verify(m => m.SaveChanges(), Times.Once);

            Assert.AreEqual(true, result);

        }

        private void SetupGameMasterData()
        {
            var data = new List<GameMaster>
            {
                new GameMaster {GameID = 1,GameName = "GameA",Description = "New Game Action",CategoryID = 1,
                    GameDetails = new List<GameDetails>(){ new GameDetails() {GameDetailsID = 1, GameID = 1,Edition = "Silver",
                        AgeRestriction = "14+",Language = "English",Platform = "PS3",Price = 25,Publisher = "EA Games",ReleasedDate = DateTime.Now } } },
                new GameMaster {GameID = 2,GameName = "GameB",Description = "New Game Adventure",CategoryID = 2,
                    GameDetails = new List<GameDetails>(){ new GameDetails() {GameDetailsID = 1, GameID = 2,Edition = "Gold",
                        AgeRestriction = "14+",Language = "English",Platform = "PS3",Price = 30,Publisher = "EA Games",ReleasedDate = DateTime.Now } } },
                new GameMaster {GameID = 3,GameName = "GameC",Description = "New Game Sports",CategoryID = 4,
                    GameDetails = new List<GameDetails>(){ new GameDetails() {GameDetailsID = 1, GameID = 3,Edition = "Diamond",
                        AgeRestriction = "14+",Language = "English",Platform = "PS4",Price = 35,Publisher = "EA Games",ReleasedDate = DateTime.Now } } }
            }.AsQueryable();

            gameMaster.As<IQueryable<GameMaster>>().Setup(m => m.Provider).Returns(data.Provider);
            gameMaster.As<IQueryable<GameMaster>>().Setup(m => m.Expression).Returns(data.Expression);
            gameMaster.As<IQueryable<GameMaster>>().Setup(m => m.ElementType).Returns(data.ElementType);
            gameMaster.As<IQueryable<GameMaster>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            mockGameContext.Setup(c => c.GameMaster).Returns(gameMaster.Object);
        }

        private void SetupGameDetailsData()
        {
            var data = new List<GameDetails>
            {
                new GameDetails() {GameDetailsID = 1, GameID = 1,Edition = "Silver",
                        AgeRestriction = "14+",Language = "English",Platform = "PS3",Price = 25,Publisher = "EA Games",ReleasedDate = DateTime.Now },

                new GameDetails() {GameDetailsID = 2, GameID = 1,Edition = "Gold",
                        AgeRestriction = "14+",Language = "English",Platform = "PS3",Price = 30,Publisher = "EA Games",ReleasedDate = DateTime.Now },

                new GameDetails() {GameDetailsID = 3, GameID = 1,Edition = "Diamond",
                        AgeRestriction = "14+",Language = "English",Platform = "PS4",Price = 35,Publisher = "EA Games",ReleasedDate = DateTime.Now }
            }.AsQueryable();


            gameDetails.As<IQueryable<GameDetails>>().Setup(m => m.Provider).Returns(data.Provider);
            gameDetails.As<IQueryable<GameDetails>>().Setup(m => m.Expression).Returns(data.Expression);
            gameDetails.As<IQueryable<GameDetails>>().Setup(m => m.ElementType).Returns(data.ElementType);
            gameDetails.As<IQueryable<GameDetails>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            mockGameContext.Setup(c => c.GameDetails).Returns(gameDetails.Object);
        }


    }
}
