﻿using NewtonAssignment.Models.DBModels;
using NewtonAssignment.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewtonAssignment.Controllers
{
    public class CategoriesController : ApiController
    {
        
        ICategoryRepository categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepo)
        {
            categoryRepository = categoryRepo;
        }

        [HttpGet]
        [Route("Category/GetCategories")]
        public List<Category> GetCategories()
        {
            return categoryRepository.GetCategories();
        }

       
    }
}