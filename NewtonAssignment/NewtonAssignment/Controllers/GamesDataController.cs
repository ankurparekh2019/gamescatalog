﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NewtonAssignment.Repository;
using NewtonAssignment.Models;
using NewtonAssignment.Models.DBModels;

namespace NewtonAssignment.Controllers
{
    public class GamesDataController : ApiController
    {
        //private GamesRepository gamesRepository = new GamesRepository();
        IGamesRepository gamesRepository;

        public GamesDataController(IGamesRepository gamesRepo)
        {
            gamesRepository = gamesRepo;
        }

        [HttpGet]
        [Route("Games/GetGameDetails")]
        public IEnumerable<GameMaster> GetGameDetails()
        {
            return gamesRepository.GetGameMasterDetails();
        }

        [HttpGet]
        [Route("Games/GetGamesByID/{id}")]
        public IHttpActionResult GetGamesByID(int id)
        {

            if (id == 0) return NotFound();

            var data = gamesRepository.GetGameMasterDetailsById(id);

            if (data == null) return NotFound();

            return Ok(data);
        }

        [HttpPost]
        [Route("Games/UpdateGameDetails")]
        // POST api/<controller>
        public IHttpActionResult Post([FromBody]GameMaster gameDetails)
        {
            if (gameDetails.GameID == 0 || !ModelState.IsValid) return BadRequest();

            var result = gamesRepository.UpdateGameDetails(gameDetails);

            if (result) return Ok(result);
            else return BadRequest();
            
        }

        [HttpPut]
        [Route("Games/AddGameDetails")]
        // PUT api/<controller>/5
        public IHttpActionResult Put([FromBody]GameMaster gameDetails)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = gamesRepository.InsertGameDetails(gameDetails);

            if (result) return Ok(result);
            else return BadRequest();
        }

        [HttpDelete]
        [Route("Games/DeleteGameMaster/{id}")]
        // DELETE api/<controller>/5
        public IHttpActionResult DeleteGameMaster(int id)
        {
            if (id == 0) return BadRequest();

            var result = gamesRepository.DeleteGameMaster(id);

            if (result) return Ok(result);
            else return NotFound();
        }

        [HttpDelete]
        [Route("Games/DeleteGameDetails/{id}")]
        // DELETE api/<controller>/5
        public IHttpActionResult DeleteGameDetails(int id)
        {
            if (id == 0) return BadRequest();

            var result = gamesRepository.DeleteGameDetails(id);

            if (result) return Ok(result);
            else return NotFound();
        }

    }
}