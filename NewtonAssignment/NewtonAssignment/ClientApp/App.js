﻿
var app = angular.module('GamesApp', ['ui.router']);

app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state("Home", {
            url: "/Home",
            templateUrl: "/ClientApp/Views/Home.html",
            controller: "HomeController"
        })
        .state("Add", {
            url: "/Add",
            templateUrl: "/ClientApp/Views/Add.html",
            controller: "AddGamesController"
        })
        .state("Edit", {
            url: "/Edit/:id",
            templateUrl: "/ClientApp/Views/Edit.html",
            controller: "EditGamesController"
        });

    // For any unmatched url, redirect to /Home
    $urlRouterProvider.otherwise("/Home");

});





