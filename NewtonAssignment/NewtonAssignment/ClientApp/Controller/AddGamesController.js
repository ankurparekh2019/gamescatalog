﻿angular
    .module('GamesApp')
    .controller('AddGamesController', AddGamesController);

AddGamesController.$inject = ['gamesDataService', '$state'];

function AddGamesController(gamesDataService, $state) {
    var addGamesCtrl = this;

    addGamesCtrl.message = "In Add Games Controller";;

    addGamesCtrl.gameID = 0;
    addGamesCtrl.gameName = '';
    addGamesCtrl.gameDesc = '';
    addGamesCtrl.selectedCategroy = 0;
    addGamesCtrl.gameDetails = [{}];

    initData();

    function initData() {
        let url = '/Category/GetCategories';

        return gamesDataService.getData(url).then(function (response) {
            console.log(response);
            var selectCategory = { CategoryID: 0, CategoryName: 'Select Category' }
            addGamesCtrl.category = response.data;
            addGamesCtrl.category.unshift(selectCategory);
            addGamesCtrl.selectedCategroy = addGamesCtrl.category[0].CategoryID;
        });
    }

    addGamesCtrl.addGameDetails = function () {
        //add more empty rows
        addGamesCtrl.gameDetails.push({});
    }

    addGamesCtrl.removeGameDetails = function (index) {
        //delete rows
        addGamesCtrl.gameDetails.splice(index, 1);
    }

    addGamesCtrl.submitGamesForm = function () {
        var gameChildDetails = addGamesCtrl.gameDetails;
        var gameDetails = { GameID: parseInt(addGamesCtrl.gameID), GameName: addGamesCtrl.gameName, Description: addGamesCtrl.gameDesc, CategoryID: addGamesCtrl.selectedCategroy, GameDetails: gameChildDetails };

        gamesDataService.insertData('/Games/AddGameDetails', JSON.stringify(gameDetails)).then(function (response) {
            if (response.data) {
                alert('Game saved successfully.');
                $state.go("Home");
            }
            else {
                alert('Error occured.');
            }

        }, function (err) {
            alert('Error occured.');

        });

    };

    addGamesCtrl.resetForm = function () {
        addGamesCtrl.gameID = 0;
        addGamesCtrl.gameName = '';
        addGamesCtrl.gameDesc = '';
        addGamesCtrl.selectedCategroy = {};
        addGamesCtrl.gameDetails = [{}];
    }
    
}

//app.controller("AddGamesController", function ($scope, $http) {
//    $scope.message = "In Add Games Controller";

//    $scope.gameID = 0;
//    $scope.gameName = '';
//    $scope.gameDesc = '';
//    $scope.selectedCategroy = 0;
//    $scope.gameDetails = [{}];
    

//    $http.get('/Category/GetCategories').then(function (response) {
//        console.log(response);
//        var selectCategory = { CategoryID: 0, CategoryName : 'Select Category'}
//        $scope.category = response.data;
//        $scope.category.unshift(selectCategory);
//        $scope.selectedCategroy = $scope.category[0].CategoryID;
//    });

//    $scope.addGameDetails = function () {
//        //add more empty rows
//        $scope.gameDetails.push({});
//    }

//    $scope.removeGameDetails = function (index) {
//        //delete rows
//        $scope.gameDetails.splice(index,1);
//    }

//    $scope.submitGamesForm = function () {
        
//        debugger;
//        var gameChildDetails = $scope.gameDetails;
//        var gameDetails = { GameID: parseInt($scope.gameID), GameName: $scope.gameName, Description: $scope.gameDesc, CategoryID: $scope.selectedCategroy, GameDetails: gameChildDetails };
        
//        $http.put('/Games/AddGameDetails', JSON.stringify(gameDetails)).then(function (response) {
//            if (response.data) {
//                alert('Game saved successfully.');
//                this.resetForm();
            
//            }
//            else {
//                alert('Error occured.');
            
//            }

//        }, function (err) {
//            alert('Error occured.');
            
//        });
           
//    };

//    $scope.resetForm = function () {
//        $scope.gameID = 0;
//        $scope.gameName = '';
//        $scope.gameDesc = '';
//        $scope.selectedCategroy = {};
//        $scope.gameDetails = [{}];
//    }


//});