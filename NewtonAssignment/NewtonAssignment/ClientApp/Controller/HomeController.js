﻿angular
    .module('GamesApp')
    .controller('HomeController', HomeController);

HomeController.$inject = ['gamesDataService', '$state'];

function HomeController(gamesDataService, $state) {
    var home = this;

    home.message = "In Home Controller";
    home.gameDetails = [];
    initData();

    function initData() {
        let url = '/Games/GetGameDetails';
        return gamesDataService.getData(url).then(function (response) {
            console.log(response);
            home.gameDetails = response.data;;
        });
        
    }

    home.deleteMaster = function (deleteId) {

        let url = '/Games/DeleteGameMaster/' + deleteId;
        gamesDataService.deleteData(url).then(function (response) {
            console.log(response);
            if (response.data) {
                alert("Master Deleted Successfully");
                $state.reload();
            }
            else {
                alert("Please Try Again");
            }
        });
    }

    home.deleteDetails = function (deleteId) {

        let url = '/Games/DeleteGameDetails/' + deleteId;
        gamesDataService.deleteData(url).then(function (response) {
            console.log(response);
            if (response.data) {
                alert("Detail Deleted Successfully");
            }
            else {
                alert("Please Try Again");
            }
        });

    }

    home.addMasterDetails = function () {
        $state.go("Add");
    }

    home.editMasterDetails = function (editId) {
        $state.go("Edit", { id: editId });
    }
    
}

//app.controller("HomeController", function ($scope, $http, $state) {
//    $scope.message = "In Home Controller";

//    $scope.gameDetails = [];
//    $http.get("/Games/GetGameDetails").then(function (response) {
//            console.log(response);
//            $scope.gameDetails = response.data;;
//        });

//    $scope.deleteMaster = function (deleteId) {

//        let url = '/Games/DeleteGameMaster/' + deleteId;
//        $http.delete(url).then(function (response) {
//            console.log(response);
//            if (response.data) {
//                alert("Master Deleted Successfully");
//                $state.reload();
//            }
//            else {
//                alert("Please Try Again");
//            }
//        });
        
//    }
    
//    $scope.deleteDetails = function (deleteId) {

//        let url = '/Games/DeleteGameDetails/' + deleteId;
//        $http.delete(url).then(function (response) {
//            console.log(response);
//            if (response.data) {
//                alert("Detail Deleted Successfully");
//            }
//            else {
//                alert("Please Try Again");
//            }
//        });

//    }

//    $scope.addMasterDetails = function () {
//        $state.go("Add");
//    }
    
//    $scope.editMasterDetails = function (editId) {
//        $state.go("Edit", { id: editId });
//    }

//});