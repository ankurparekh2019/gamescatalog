﻿angular
    .module('GamesApp')
    .controller('EditGamesController', EditGamesController);

EditGamesController.$inject = ['gamesDataService', '$state', '$stateParams'];

function EditGamesController(gamesDataService, $state, $stateParams) {

    var editGamesCtrl = this;

    editGamesCtrl.message = "In Edit Games Controller";

    editGamesCtrl.id = $stateParams.id;
    editGamesCtrl.selectedGame = {};

    initData();

    function initData() {
        let url = '/Category/GetCategories';

        return gamesDataService.getData(url).then(function (response) {
            console.log(response);
            editGamesCtrl.category = response.data;
            let url = '/Games/GetGamesByID/' + editGamesCtrl.id;
            return gamesDataService.getData(url).then(function (response) {
                console.log(response);
                editGamesCtrl.selectedGame = response.data;
            });
            
        });
    }

    editGamesCtrl.addGameDetails = function () {
        //add more empty rows
        editGamesCtrl.selectedGame.GameDetails.push({});
    }

    editGamesCtrl.removeGameDetails = function (index) {
        //delete rows
        editGamesCtrl.selectedGame.GameDetails.splice(index, 1);
    }

    editGamesCtrl.updateGame = function () {
        //debugger;
        //var gameDetails = { GameDetailsID: parseInt($scope.gameID), Name: $scope.gameName };

        gamesDataService.updateData('/Games/UpdateGameDetails', JSON.stringify(editGamesCtrl.selectedGame)).then(function (response) {
            if (response.data) {
                alert('Game Updated successfully.');
                editGamesCtrl.selectedGame = {};
                $state.go("Home");
            }
            else {
                alert('Error occured.');
            }

        }, function (err) {
            alert('Error occured.');
        });

    };

    editGamesCtrl.resetForm = function () {
        $state.go("Home");
    }
}




//app.controller("EditGamesController", function ($scope, $http, $state, $stateParams) {
//    $scope.message = "In Edit Games Controller";

//    $scope.id = $stateParams.id;
//    $scope.selectedGame = {};
    

//    let url = '/Games/GetGamesByID/' + $scope.id;
//    $http.get(url).then(function (response) {
//        console.log(response);
//        $scope.selectedGame = response.data;
//    });

//    $http.get('/Category/GetCategories').then(function (response) {
//        console.log(response);
//        $scope.category = response.data;
//    });

//    $scope.addGameDetails = function () {
//        //add more empty rows
//        $scope.selectedGame.GameDetails.push({});
//    }

//    $scope.removeGameDetails = function (index) {
//        //delete rows
//        $scope.selectedGame.GameDetails.splice(index, 1);
//    }

//    $scope.updateGame = function () {
//        debugger;
//        //var gameDetails = { GameDetailsID: parseInt($scope.gameID), Name: $scope.gameName };
        
//        $http.post('/Games/UpdateGameDetails', JSON.stringify($scope.selectedGame)).then(function (response) {
//            if (response.data) {
//                alert('Game Updated successfully.');
//                $scope.selectedGame = {};
//                $scope.disableSubmit = false;
//                $state.go("Home");
//            }
//            else {
//                alert('Error occured.');
////                $scope.disableSubmit = false;
//            }

//        }, function (err) {
//            alert('Error occured.');
//            //$scope.disableSubmit = false;
//        });

//    };

//    $scope.resetForm = function () {
//        $state.go("Home");
//    }

//});

//app.directive('inputDate', function () {
//    return {
//        restrict: 'A',
//        scope: {
//            ngModel: '='
//        },
//        link: function (scope) {
//            if (scope.ngModel) scope.ngModel = new Date(scope.ngModel);
//        }
//    }
//});
