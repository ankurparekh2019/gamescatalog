﻿angular.module('GamesApp').directive('inputDate', inputDate);

function inputDate() {
    return {
        restrict: 'A',
        scope: {
            ngModel: '='
        },
        link: function (scope) {
            if (scope.ngModel) scope.ngModel = new Date(scope.ngModel);
        }
    }
}