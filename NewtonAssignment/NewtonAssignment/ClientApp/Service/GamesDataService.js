﻿angular.module('GamesApp')
    .factory('gamesDataService', gamesDataService);

gamesDataService.$inject = ['$http',];

function gamesDataService($http) {
    return {
        getData: getData,
        getDataById : getData,
        updateData: updateData,
        insertData: insertData,
        deleteData: deleteData
    };

    function getData(url) {
        return $http.get(url)
            .then(function (response) {
                // return the result as a promise
                return response;
            }, function (response) {
                // defer the promise
                return $q.reject(response.data);
            });
    }

    function updateData(url,params) {
        return $http.post(url, params)
            .then(function (response) {
                // return the result as a promise
                return response;
            }, function (response) {
                // defer the promise
                return $q.reject(response.data);
            });
    }


    function insertData(url, params) {
        return $http.put(url, params)
            .then(function (response) {
                // return the result as a promise
                return response;
            }, function (response) {
                // defer the promise
                return $q.reject(response.data);
            });
    }

    function deleteData(url) {
        return $http.delete(url)
            .then(function (response) {
                // return the result as a promise
                return response;
            }, function (response) {
                // defer the promise
                return $q.reject(response.data);
            });
    }
    
}