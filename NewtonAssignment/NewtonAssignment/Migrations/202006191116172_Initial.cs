﻿namespace NewtonAssignment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.GameMasters",
                c => new
                    {
                        GameID = c.Int(nullable: false, identity: true),
                        GameName = c.String(),
                        Description = c.String(),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameID)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.GameDetails",
                c => new
                    {
                        GameDetailsID = c.Int(nullable: false, identity: true),
                        Edition = c.String(),
                        ReleasedDate = c.DateTime(nullable: false),
                        AgeRestriction = c.String(),
                        Language = c.String(),
                        Publisher = c.String(),
                        Platform = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GameID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameDetailsID)
                .ForeignKey("dbo.GameMasters", t => t.GameID, cascadeDelete: true)
                .Index(t => t.GameID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GameDetails", "GameID", "dbo.GameMasters");
            DropForeignKey("dbo.GameMasters", "CategoryID", "dbo.Categories");
            DropIndex("dbo.GameDetails", new[] { "GameID" });
            DropIndex("dbo.GameMasters", new[] { "CategoryID" });
            DropTable("dbo.GameDetails");
            DropTable("dbo.GameMasters");
            DropTable("dbo.Categories");
        }
    }
}
