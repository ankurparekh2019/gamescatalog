﻿namespace NewtonAssignment.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NewtonAssignment.Models.GamesDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(NewtonAssignment.Models.GamesDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            context.Category.AddOrUpdate(a => a.CategoryID,
               new Models.DBModels.Category { CategoryID = 1, CategoryName = "Action" },
               new Models.DBModels.Category { CategoryID = 2, CategoryName = "Adventure" },
               new Models.DBModels.Category { CategoryID = 3, CategoryName = "Simulation" },
               new Models.DBModels.Category { CategoryID = 4, CategoryName = "Sports" },
               new Models.DBModels.Category { CategoryID = 5, CategoryName = "Strategy" },
               new Models.DBModels.Category { CategoryID = 6, CategoryName = "Puzzle" });
        }
    }
}
