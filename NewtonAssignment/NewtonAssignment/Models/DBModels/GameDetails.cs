﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NewtonAssignment.Models.DBModels
{
    public class GameDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GameDetailsID { get; set; }

        public string Edition { get; set; }

        public DateTime ReleasedDate { get; set; }

        public string AgeRestriction { get; set; }

        public string Language { get; set; }

        public string Publisher { get; set; }

        public string Platform { get; set; }

        public decimal Price { get; set; }

//        public byte[] Icon { get; set; }
       
        public int GameID { get; set; }

        [ForeignKey("GameID")]
        public virtual GameMaster GameMaster { get; set; }

        
        
    }
}