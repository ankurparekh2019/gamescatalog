﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using NewtonAssignment.Models.DBModels;

namespace NewtonAssignment.Models
{
    public class GamesDBContext : DbContext 
    {
        public GamesDBContext() : base("DefaultConnection") { }

        public virtual DbSet<GameMaster> GameMaster { get; set; }

        public virtual DbSet<GameDetails> GameDetails { get; set; }

        public virtual DbSet<Category> Category { get; set; }

        //public DbSet<Publishers> Publishers { get; set; }

        //public DbSet<GamePlatforms> GamePlatforms { get; set; }
    }
}