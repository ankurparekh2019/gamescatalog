﻿using NewtonAssignment.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NewtonAssignment.Repository
{
   public interface IGamesRepository
    {
        List<GameMaster> GetGameMasterDetails();

         GameMaster GetGameMasterDetailsById(int id);

        bool InsertGameDetails(GameMaster gameDetails);

        bool UpdateGameDetails(GameMaster gameDetails);

        bool DeleteGameMaster(int GameMasterID);

        bool DeleteGameDetails(int GameDetailsID);
    }
}
