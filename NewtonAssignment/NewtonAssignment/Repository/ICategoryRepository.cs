﻿using NewtonAssignment.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewtonAssignment.Repository
{
    public interface ICategoryRepository
    {
        List<Category> GetCategories();
    }
}
