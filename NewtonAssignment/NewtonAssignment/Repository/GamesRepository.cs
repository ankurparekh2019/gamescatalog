﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NewtonAssignment.Models;
using NewtonAssignment.Models.DBModels;

namespace NewtonAssignment.Repository
{
    public class GamesRepository : IGamesRepository
    {
        //private GamesDBContext context = new GamesDBContext();

        private GamesDBContext context;

        public GamesRepository(GamesDBContext gamesDBContext)
        {
            context = gamesDBContext;
        }

        public List<GameMaster> GetGameMasterDetails()
        {
            try
            {
                // var data = context.GameMaster.Include("GameDetails").Include("GameDetails.Category").ToList();
                var data = context.GameMaster.ToList();
                return data;
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError("Error In Getting Game Details");
                return null;
            }
        }

        public GameMaster GetGameMasterDetailsById(int id)
        {
            try
            {
                //var data = context.GameMaster.Include("GameDetails").Include("Category").ToList().Where(a => a.GameID == id).FirstOrDefault();
                var data = GetGameMasterDetails().FirstOrDefault(a => a.GameID == id);
                return data;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error In Getting Game Details By ID");
                return null;
            }
        }

        public bool InsertGameDetails(GameMaster gameDetails)
        {

            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var gameMasterDetails = new GameMaster()
                    {
                        Description = gameDetails.Description,
                        GameName = gameDetails.GameName,
                        CategoryID = gameDetails.CategoryID
                    };
                    context.GameMaster.Add(gameMasterDetails);
                    context.SaveChanges();
                    //gameDetails.GameDetails.ForEach(a => { a.GameID = gameDetails.GameID; });
                    if (gameDetails.GameDetails.Any())
                    {
                        foreach (var item in gameDetails.GameDetails)
                        {
                            var gameChildDetails = new GameDetails()
                            {
                                GameID = gameMasterDetails.GameID,
                                Edition = item.Edition,
                                ReleasedDate = item.ReleasedDate,
                                AgeRestriction = item.AgeRestriction,
                                Language = item.Language,
                                Platform = item.Platform,
                                Price = item.Price,
                                Publisher = item.Publisher
                            };
                            context.GameDetails.Add(gameChildDetails);
                        }
                        context.SaveChanges();
                    }
                    transaction.Commit();
                    return true;
                    
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    System.Diagnostics.Trace.TraceError("Error In Inserting Game Details");
                    return false;
                }
            }

        }

        public bool UpdateGameDetails(GameMaster gameDetails)
        {

            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var gameMaster = context.GameMaster.FirstOrDefault(a => a.GameID == gameDetails.GameID);
                    if (gameMaster == null) return false;

                    gameMaster.GameName = gameDetails.GameName;
                    gameMaster.Description = gameDetails.Description;
                    gameMaster.CategoryID = gameDetails.CategoryID;

                    //Update the Entries where GameID matches with the details game master id
                    var gameDetailsToUpdate = gameDetails.GameDetails.Where(a => a.GameID == gameDetails.GameID).ToList();
                    foreach (var item in gameDetailsToUpdate)
                    {
                        var existingGameDetails = gameMaster.GameDetails.FirstOrDefault(a => a.GameDetailsID == item.GameDetailsID);
                        existingGameDetails.ReleasedDate = item.ReleasedDate;
                        existingGameDetails.Edition = item.Edition;
                        existingGameDetails.AgeRestriction = item.AgeRestriction;
                        existingGameDetails.Language = item.Language;
                        existingGameDetails.Publisher = item.Publisher;
                        existingGameDetails.Platform = item.Platform;
                        existingGameDetails.Price = item.Price;
                    }

                    var existingGameDetailsList = context.GameDetails.Where(a => a.GameID == gameDetails.GameID).ToList();

                    //Delete Game Details which is not present in the gameDetails Paramater and Gamedetails id is not 0
                    var gameDetailsToDelete = existingGameDetailsList.Where(a => !gameDetails.GameDetails.Select(b => b.GameDetailsID).ToList().Contains(a.GameDetailsID)).ToList();
                    if (gameDetailsToDelete != null && gameDetailsToDelete.Count() > 0)
                        context.GameDetails.RemoveRange(gameDetailsToDelete);

                    //Insert Game Details which is not present in the gameDetails Database list and Gamedetails id 0
                    var gameDetailsToInsert = gameDetails.GameDetails.Where(a => !existingGameDetailsList.Select(b => b.GameDetailsID).ToList().Contains(a.GameDetailsID)).ToList();
                    if (gameDetailsToInsert != null && gameDetailsToInsert.Count() > 0)
                    {
                        gameDetailsToInsert.ToList().ForEach(a => { a.GameID = gameDetails.GameID; });
                        context.GameDetails.AddRange(gameDetailsToInsert);
                    }
                    context.SaveChanges();
                    transaction.Commit();
                    return true;
                }


                catch (Exception ex)
                {
                    transaction.Rollback();
                    System.Diagnostics.Trace.TraceError("Error in Updating Game Details");
                    return false;
                }
            }

        }

        public bool DeleteGameDetails(int GameDetailsID)
        {

            try
            {
                var gameDetailToRemove = context.GameDetails.FirstOrDefault(a => a.GameDetailsID == GameDetailsID);

                if (gameDetailToRemove == null) return false;

                context.GameDetails.Remove(gameDetailToRemove);
                context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError("Error in Deleting Game Details");
                return false;
            }

        }

        public bool DeleteGameMaster(int GameMasterID)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var gameMasterToRemove = context.GameMaster.FirstOrDefault(a => a.GameID == GameMasterID);

                    if (gameMasterToRemove == null) return false;

                    var gameDetailsToRemove = context.GameDetails.Where(a => a.GameID == GameMasterID).ToList();

                    context.GameDetails.RemoveRange(gameDetailsToRemove);

                    context.GameMaster.Remove(gameMasterToRemove);
                    context.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    System.Diagnostics.Trace.TraceError("Error in Deleting Game Master");
                    return false;
                }
            }
        }
    }
}