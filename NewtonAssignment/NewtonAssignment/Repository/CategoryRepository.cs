﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NewtonAssignment.Models;
using NewtonAssignment.Models.DBModels;

namespace NewtonAssignment.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private GamesDBContext context;

        public CategoryRepository(GamesDBContext gamesDBContext)
        {
            context = gamesDBContext;
        }

        public List<Category> GetCategories()
        {
            var data = context.Category.ToList();
            return data;
        }
    }
}